﻿/*
 *  Arel Jann Clemente | 991436441 | Assignment 1 | Web Services Using .NET & C#
 *  
 *  This is the Employee class that is used to create an employee object.
 */

using System;

namespace Assignment1
{
    class Employee
    {
        protected string Id { get; set; }
        protected string Name { get; set; }
        protected string Profession { get; set; }

        public Employee(string id, string name, string profession) 
        {
            Id = id;
            Name = name;
            Profession = profession;
        }

        public virtual void EmployeeDetails()
        {
            Console.WriteLine($"ID: {Id}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Profession: {Profession}");
        }
    }
}
