﻿/*
 *  Arel Jann Clemente | 991436441 | Assignment 1 | Web Services Using .NET & C#
 *  
 *  This is a test class for the Employee and SelfEmployee classes.
 *  This program will ask user to input employee details and is then displayed on output.
 */ 

using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1
{
    class Test
    {
        static void Main(string[] args)
        {
            bool input_complete = false;                                // if user does not want to add another employee, this will be true
            string add_employee;                                        // asks user to input 'Y' or 'N' if they want to add employee to list
            string employee_id = "";
            string employee_name = "";
            string employee_profession = "";
            double employee_hourly_rate = 0.00;
            double employee_total_hours = 0.00;
            List<Employee> employees = new List<Employee>();            // use a List<T> to store employee objects

            do
            {
                Console.Write("Enter employee ID: ");                   // ask user for employee ID
                employee_id = Console.ReadLine();                           // get input from user

                Console.Write("Enter employee name: ");                 // ask user for employee name
                employee_name = Console.ReadLine();                         // get input from user
           
                Console.Write("Enter employee profession: ");           // ask user for employee profession
                employee_profession = Console.ReadLine();                   // get input from user

                Console.Write("Enter employee hourly rate: ");          // ask user for employee hourly rate
                employee_hourly_rate = double.Parse(Console.ReadLine());    // get input from user

                Console.Write("Enter employee total hours: ");          // ask user for employee ID
                employee_total_hours = double.Parse(Console.ReadLine());    // get input from user

                employees.Add(new SelfEmployee(employee_id,             // adds employee to employee
                    employee_name, employee_profession, 
                    employee_hourly_rate, employee_total_hours));

                // do-while loop is used here for input validation when user is asked 
                // if they would like to add another to the list
                do
                {
                    Console.Write("Add another employee? (Y/N): ");     // ask user if they want to add another employee to the list
                    add_employee = Console.ReadLine().ToLower();            // get input from user; input is converted to lower case
                    if (add_employee != "y" && add_employee != "n")     // if user inputs something other than 'Y' or 'N', it will notify user input is incorrect
                    {
                        Console.Write("Incorrect input.\n");
                    }
                    else if (add_employee == "n")                       // if user inputs 'N', this loop will end and employee list will be displayed
                    {
                        input_complete = true;                          // sets input_complete to true, meaning user does not want to add another employee
                    }
                    else if (add_employee == "y")                       // if user inputs 'Y', the entire process of adding new employee will occur
                    {
                        input_complete = false;                         // sets input_complete to false, meaning user wants to add another employee
                    }

                } while (add_employee != "y" && add_employee != "n");   // while user input is not 'y' or 'n', ask user again until input is valid
            } while (!input_complete);                                  // if input_complete is true, adding employee process will end
            
            // this for-loop displays all the employees' details in the list
            for(int i = 0; i < employees.Count; i++)
            {
                Console.WriteLine("");
                employees[i].EmployeeDetails();
            }

            // prevents program from terminating right away; we want to see the output first
            Console.ReadKey();
        }
    }
}
