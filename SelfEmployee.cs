﻿/*
 *  Arel Jann Clemente | 991436441 | Assignment 1 | Web Services Using .NET & C#
 *  
 *  This is the SelfEmployee class that is derived from the Employee class.
 *  This class has added attributes such as employee hourly rate, total hours,
 *  and calculated wage.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1
{
    class SelfEmployee : Employee
    {
        protected double HourlyRate { get; set; }
        protected double TotalHours { get; set; }
        protected double Wage { get; set; }

        public SelfEmployee(string id, string name, string profession) : base(id, name, profession)
        {
        }

        public SelfEmployee(string id, string name, string profession, double hourlyRate, double totalHours) : base(id, name, profession)
        {
            HourlyRate = hourlyRate;
            TotalHours = totalHours;
        }

        public override void EmployeeDetails()
        {
            Console.WriteLine($"ID: {Id}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Profession: {Profession}");
            Console.WriteLine($"Hourly Rate: {HourlyRate}");
            Console.WriteLine($"Total Hours: {TotalHours}");
            Console.WriteLine($"Wage: ${CalculateWage()}");
        }

        public double CalculateWage()
        {
            Wage = HourlyRate * TotalHours;
            return Wage;
        }
    }
}
